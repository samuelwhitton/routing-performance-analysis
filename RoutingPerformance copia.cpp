//
//  RoutingPerformance.cpp
//  Routing Performance
//
//  Created by Samuel Whitton on 5/10/2014.
//  Copyright (c) 2014 Samuel Whittion. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>


namespace std {
	
	typedef enum _networkScheme {
        CIRCUIT,
        PACKET
    } NetworkScheme;
    
    typedef enum _routingScheme {
        SHP,    //shortest hop path
        SDP,    //shortest delay path
        LLP     //least loaded path
    } RoutingScheme;
    
    //a single unit/load of work
    typedef struct _work {
        string src, dest;
        double startSec, timeSec;
    } Work;
    
    class NetworkTopology;
	
}


class std::NetworkTopology {
    
    
public:
    NetworkTopology() {}
    
    void addRouterLink(string router1, string router2, int propMS, int capacity) {
        
    }
    
};



int main(int argc, const char * argv[]) {
    
    /* 1. parse parameters */
    
	std::string strUsage = "Usage: RoutingPerformance [NETWORK_SCHEME: CIRCUIT or PACKET] [ROUTING_SCHEME: SHP, SDP or LLP] TOPOLOGY_FILE WORKLOAD_FILE PACKET_RATE\n";
    
    if (argc != 6) {
    	std::cout << strUsage;
        return EXIT_FAILURE;
    }
    
    std::NetworkScheme network;
    std::RoutingScheme routing;
    
    if (std::string(argv[1]).compare("CIRCUIT") == 0) {
        network = std::CIRCUIT;
    } else if (std::string(argv[1]).compare("PACKET") == 0) {
        network = std::PACKET;
    } else {
        std::cout << strUsage;
        return EXIT_FAILURE;
    }
    
    if (std::string(argv[2]).compare("SHP") == 0) {
        routing = std::SHP;
    } else if (std::string(argv[2]).compare("SDP") == 0) {
        routing = std::SDP;
    } else if (std::string(argv[2]).compare("LLP") == 0) {
        routing = std::LLP;
    } else {
        std::cout << strUsage;
        return EXIT_FAILURE;
    }
    
    std::string strTopologyFile = std::string(argv[3]);
    std::string strWorkloadFile = std::string(argv[4]);
    int packetRate = std::atoi(argv[5]);
    
    if ( !(packetRate > 0) ) {
        std::cout << "Packet rate must be a positive integer.\n";
    }
    
    /* 2. parse topology file, creating network topology */
    
    std::NetworkTopology topology = std::NetworkTopology();
    
    std::ifstream topologyFile(strTopologyFile, std::ios::in);
	
    if (!topologyFile.is_open()) {
        std::cout << "Topology file \"" + strTopologyFile + "\" could not be opened.\n";
        return EXIT_FAILURE;
    }
    
    std::string topologyLine;
    while (std::getline(topologyFile, topologyLine, '\n')) {
        //parse each line of the topology file adding each router link
        std::istringstream topologyStream(topologyLine);
        std::string router1, router2;
        int propMS, capacity;
        topologyStream >> router1 >> router2 >> propMS >> capacity;
        topology.addRouterLink(router1, router2, propMS, capacity);
    }
    
    /* 3. parse workload file */
    
    std::vector<std::Work> workload = std::vector<std::Work>();
    
    std::ifstream workloadFile(strWorkloadFile, std::ios::in);
	
    if (!workloadFile.is_open()) {
        std::cout << "Workload file \"" + strTopologyFile + "\" could not be opened.\n";
        return EXIT_FAILURE;
    }
    
    std::string workloadLine;
    while (std::getline(workloadFile, workloadLine, '\n')) {
        //parse each line of the workload into vector array
        std::istringstream workloadStream(workloadLine);
        std::string src, dest;
        double startSec, timeSec;
        workloadStream >> startSec >> src >> dest >> timeSec;
        std::Work w = {src, dest, startSec, timeSec};
        workload.push_back(w);
    }
    
    
    return EXIT_SUCCESS;
}












