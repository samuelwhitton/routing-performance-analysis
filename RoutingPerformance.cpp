//
//  RoutingPerformance.cpp
//  Routing Performance
//
//  Course: COMP3331 - Networks and Applications
//  Assignment: Routing Performance Analysis
//  Author: Samuel Whitton (z3461420)
//
//  Created by Samuel Whitton on 5/10/2014.
//  Copyright (c) 2014 Samuel Whittion. All rights reserved.
//
//  Compiles with g++ and no extensions on the lab computers
//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include <math.h>
#include <map>
#include <algorithm>


namespace std {
	
    typedef enum _networkScheme {
        CIRCUIT,
        PACKET
    } NetworkScheme;
    
    typedef enum _routingScheme {
        SHP,    //shortest hop path
        SDP,    //shortest delay path
        LLP     //least loaded path
    } RoutingScheme;
    
    //a single unit/load of work
    struct Work {
        string src, dest;
        double startSec, timeSec;
    };
    
    //describes a connection to another router, returned from std::NetworkTopology::getConnectedRouters
    struct RouterConn {
        RouterConn() {}
        RouterConn(string router, double load, int propMS) : router(router), load(load), propMS(propMS) {}
        string router;
        double load;
        int propMS;
    };

    //return data from netwrok analysis, stats etc
    struct NetworkStats {
        int vcircuitRequests;
        int vcircuitRouted;
        int packetsRouted;
        int packetsBlocked;
        int totalRoutedVCircuitHops;
        int totalRoutedVCircuitProp;
    };
    
    class NetworkTopology; //represents the topology of the network
    class NetworkAnalysis; //analyses a type of network using a routing algorithm against a given workload
    class RoutingAlgorithm; //all routing algorithms are of this type
    
    //routing algorithms
    class RoutingSHP;
    class RoutingSDP;
    class RoutingLLP;
    
}


//represents the topology of the network
//manages the resources in the network
class std::NetworkTopology {
    
    //represents a link between two routers
    struct RouterLink {
        RouterLink() {}
        RouterLink(int totalCapacity, int usedCapacity, int propMS) : totalCapacity(totalCapacity), usedCapacity(usedCapacity), propMS(propMS) {}
        int totalCapacity;
        int usedCapacity;
        int propMS;
    };
    
private:
    int nextVCID;
    
    //each connection of two routers
    //map<pair<router1, router2>>
    map<pair<string, string>, RouterLink> routerConnections;
    
    //a map to find a vector of connections for each router
    //map<router, vector<pair<connected router, RouterLink *>>>
    map<string, vector<pair<string, RouterLink *> > > connectionsFromRouter;
    
    //a map to contain the RouterLinks for each VC connection so we can release them later
    map<int, vector<RouterLink *> > vcLinkRoutes;
    
    //router pair must always be ordered the same
    static pair<string, string> getRouterPair(string router1, string router2) {
        if (router1.compare(router2) < 0)
            return pair<string, string>(router2, router1);
        return pair<string, string>(router1, router2);
    }
    
public:
    NetworkTopology() {
        nextVCID = 3;
    }
    
    void addRouterConnection(string router1, string router2, int propMS, int capacity) {
        pair<string, string> routerPair = getRouterPair(router1, router2);
        RouterLink link = RouterLink(capacity, 0, propMS);
        routerConnections[routerPair] = link;
        
        //add new connection to both router1 and router2
        RouterLink *linkAddr = &routerConnections[routerPair];
        connectionsFromRouter[router1].push_back(pair<string, RouterLink *>(router2, linkAddr));
        connectionsFromRouter[router2].push_back(pair<string, RouterLink *>(router1, linkAddr));
    }

    //get all the connected routers to a given router, including the:
    //name, propogation delay and the load on the connection
    vector<RouterConn> getConnectedRouters(string router) {
        vector<RouterConn> connected;
        for (int i = 0; i < connectionsFromRouter[router].size(); i++) {
            pair<string, RouterLink *> conn = connectionsFromRouter[router][i];
            connected.push_back(RouterConn(conn.first, (double)conn.second->usedCapacity / (double)conn.second->totalCapacity, conn.second->propMS));
        }
        return connected;
    }
    
    //get connection info between two routers
    RouterConn getRouterConn(string router1, string router2) {
        RouterLink link = routerConnections[getRouterPair(router1, router2)];
        return RouterConn("", (double)link.usedCapacity / (double)link.totalCapacity, link.propMS);
    }
    
    //get a unique id for a new VC conection
    int getVCID() {
        int vcID = nextVCID;
        nextVCID++;
        return vcID;
    }
    
    //release a VC from the network
    void releaseVC(int vcID) {
        //first check that vc is established
        if (isEstablishedVC(vcID)) {
            for (int i = 0; i < vcLinkRoutes[vcID].size(); i++) {
                RouterLink *link = vcLinkRoutes[vcID][i];
                //release resources at each link
                link->usedCapacity -= 1;
            }
            //delete route
            vcLinkRoutes.erase(vcID);
        }
    }
    
    //request a VC from the network, returns true if resources avaliable on a given path
    bool requestVC(int vcID, vector<string> route) {
        //check that vc is avaliable, return false if any link is full
        for (int i = 1; i < route.size(); i++) {
            RouterLink link = routerConnections[getRouterPair(route[i-1], route[i])];
            if (link.totalCapacity == link.usedCapacity)
                return false; //fully booked!
        }
        
        //VC avaliable, book the route
        vector<RouterLink *> bookedRoute;
        for (int i = 1; i < route.size(); i++) {
            RouterLink *link = &routerConnections[getRouterPair(route[i-1], route[i])];
            link->usedCapacity += 1;
            bookedRoute.push_back(link);
        }
        vcLinkRoutes[vcID] = bookedRoute;
    
        return true;
    }
    
    //check if a given vc is an established connection
    bool isEstablishedVC(int vcID) {
        return vcLinkRoutes.find(vcID) != vcLinkRoutes.end();
    }
    
    //sum propogation delay of route for VC
    int sumRoutePropDelayMS(int vcID) {
        int propMS = 0;
        for (int i = 0; i < vcLinkRoutes[vcID].size(); i++) {
            RouterLink *link = vcLinkRoutes[vcID][i];
            propMS += link->propMS;
        }
        return propMS;
    }
    
    //sum number of hops in routefor VC
    int sumRouteHops(int vcID) {
        return (int)vcLinkRoutes[vcID].size();
    }
    
};


namespace std {
    
    //resolves node comparisons arbitrairily
    static long arbitraryResolver = 0;
    
    //Represents a node and the cost to such a node.
    //Nodes are also ordered from smallest to greatest cost.
    //Used exclusively by std::RoutingAlgorithm but defined outside a local
    //scope for use as a template argument.
    struct Node {
        double cost;
        string node;
        string fromNode;
        Node() : cost(0.0), node(""), fromNode("") {
            arbitraryResolver++;
            arbitraryResolutionID = arbitraryResolver;
        }
        Node(double cost, string node, string fromNode) : cost(cost), node(node), fromNode(fromNode) {
            arbitraryResolver++;
            arbitraryResolutionID = arbitraryResolver;
        }
        bool operator<(const struct Node& other) const {
            if (cost == other.cost) {
                //resolving arbitrarily
                return arbitraryResolutionID > other.arbitraryResolutionID;
            }
            return cost > other.cost;
        }
    private:
        long arbitraryResolutionID;
    };
}



//implements Dijkstra's algorithm, all routing algorithms inherit from this
class std::RoutingAlgorithm {
protected:
    NetworkTopology *topology;
    
    //add a link cost to an existing routing path cost
    //overriden by subclass to determine the routing protocol used
    virtual double addLinkCost(double currentCost, string router1, string router2) {
        return currentCost + 1.0; //each link is a hop (default, ignore this)
    }
    
public:
    
    RoutingAlgorithm(NetworkTopology *topology) {
        this->topology = topology;
    }
    
    //for finding the route we assume there is a route,
    //i.e. we don't need to check that a route exists
    vector<string> route(string src, string dest) {
        
        // *** Dijkstra's algorithm ***
        
        //map of the cost to and node this node was reached from, for each visited node
        map<string, Node> visitedNodes;
        
        //nodes which can be reached at on the current search boundary
        //priority queue, the smallest cost nodes are processed first
        priority_queue<Node, vector<Node> > boundary;
        
        Node currNode = Node(0.0, src, "");
        visitedNodes[src] = currNode;
        
        while (currNode.node.compare(dest) != 0) {
            
            //add each non visited node to boundary, along with new cost etc
            vector<RouterConn> conns = topology->getConnectedRouters(currNode.node);
            for (int i = 0; i < conns.size(); i++) {
                RouterConn conn = conns[i];
                if (visitedNodes.find(conn.router) == visitedNodes.end()) {
                    boundary.push( Node(addLinkCost(currNode.cost, currNode.node, conn.router), conn.router, currNode.node) );
                }
            }
            
            //find lowest cost node as next node
            currNode = boundary.top();
            boundary.pop();
            visitedNodes[currNode.node] = currNode; //add visited
        }
        
        //form path vector by going backwards through the Nodes to find the path and reversing it
        vector<string> path;
        while (currNode.node.compare(src) != 0) {
            path.push_back(currNode.node);
            currNode = visitedNodes[currNode.fromNode];
        }
        path.push_back(src);
        reverse(path.begin(), path.end());
        
        return path;
    }
};


class std::RoutingSHP : public RoutingAlgorithm {
private:
    double addLinkCost(double currentCost, string router1, string router2) {
        //each link is added as a single hop
        return currentCost + 1.0;
    }
public:
    RoutingSHP(NetworkTopology *topology) : RoutingAlgorithm(topology) {}
};

class std::RoutingSDP : public RoutingAlgorithm {
private:
    double addLinkCost(double currentCost, string router1, string router2) {
        //each link adds its propogation delay
        return currentCost + (double)topology->getRouterConn(router1, router2).propMS;
    }
public:
    RoutingSDP(NetworkTopology *topology) : RoutingAlgorithm(topology) {}
};

class std::RoutingLLP : public RoutingAlgorithm {
private:
    double addLinkCost(double currentCost, string router1, string router2) {
        //if the new link has a greater load, it becomes the new cost
        double load = topology->getRouterConn(router1, router2).load;
        return (load > currentCost) ? load : currentCost;
    }
public:
    RoutingLLP(NetworkTopology *topology) : RoutingAlgorithm(topology) {}
};


//represents a single action, reserving or releasing a virtual connection, or routing a packet through a VC
//used exclusively by std::NetworkAnalysis
namespace std { struct RouteAction;}
struct std::RouteAction {
    typedef enum _actionType {
        ReserveVC,
        ReleaseVC,
        SendPacketVC
    } ActionType;
private:
    RouteAction(ActionType type, double time, int vcID, vector<string> route) : type(type), time(time), vcID(vcID), route(route) {}
public:
    ActionType type; //type of action
    int vcID;
    vector<string> route;
    double time; //time this action should take place
    
    static RouteAction reserveVC(double time, int vcID, vector<string> route) {
        return RouteAction(ReserveVC, time, vcID, route); }
    static RouteAction releaseVC(double time, int vcID) {
        return RouteAction(ReleaseVC, time, vcID, vector<string>()); }
    static RouteAction sendPacket(double time, int vcID) {
        return RouteAction(SendPacketVC, time, vcID, vector<string>()); }
    
    //releasing has priority, then reserving, then sending packets
    //smallest time order has the biggest priority though
    bool operator<(const struct RouteAction& other) const {
        if (time != other.time) {
            return time > other.time;
        }
        return  (   (other.type == ReleaseVC)
                ||  (other.type == ReserveVC && type != ReleaseVC)
                ||  (type == SendPacketVC)         );
    }
};


//where the core of the routing performance program takes place
//analyses a type of network using a routing algorithm against a given workload
class std::NetworkAnalysis {
public:
    static NetworkStats analyseRoutingPerformance(NetworkTopology topology, NetworkScheme networkScheme, RoutingScheme routingScheme, vector<Work> workload, int packetRate) {
        
        //use selected routing algorithm
        RoutingAlgorithm *routingAlgorithm = NULL;
        if (routingScheme == std::SHP)
            routingAlgorithm = new RoutingSHP(&topology);
        else if (routingScheme == std::SDP)
            routingAlgorithm = new RoutingSDP(&topology);
        else if (routingScheme == std::LLP)
            routingAlgorithm = new RoutingLLP(&topology);
        
        /*
         each workload item will be processed and actions will be populated
         on the actionQueue. At the same time actions will be processed from the queue,
         but only when the next action time is less then the next Work startSec.
         Stats will be updated based on the result of each action.
         */
        
        NetworkStats stats = {0,0,0,0,0,0};
        priority_queue<RouteAction, vector<RouteAction> > actionQueue;
        int workloadPos = 0;
        
        //continue while there is work to be done
        while (actionQueue.size() > 0 || workloadPos < workload.size()) {
            
            if (actionQueue.size() > 0) {
                if (workloadPos == workload.size()
                    || (actionQueue.top().time < workload[workloadPos].startSec)) {
                    
                    //next actionQueue action should be processed
                    RouteAction act = actionQueue.top();
                    actionQueue.pop();
                    
                    if (act.type == RouteAction::SendPacketVC) {
                        if (topology.isEstablishedVC(act.vcID)) {
                            //successful packet routing
                            stats.packetsRouted += 1;
                        } else {
                            stats.packetsBlocked += 1;
                        }
                    
                    } else if (act.type == RouteAction::ReserveVC) {
                        stats.vcircuitRequests += 1;
                        if (topology.requestVC(act.vcID, act.route)) {
                            //successfull VC routing
                            stats.vcircuitRouted += 1;
                            stats.totalRoutedVCircuitHops += topology.sumRouteHops(act.vcID);
                            stats.totalRoutedVCircuitProp += topology.sumRoutePropDelayMS(act.vcID);
                        }
                        
                    } else if (act.type == RouteAction::ReleaseVC) {
                        topology.releaseVC(act.vcID); //release the VC resources
                    }
                    
                    continue;
                }
            }
            
            //next workload Work item should be processed
            Work work = workload[workloadPos];
            
            //calculate the number of packets to be sent on this work interval
            //depends on packetRate (per sec) and the work timeSec
            int numPackets = (int)( ceil( work.timeSec * (double)packetRate ) );
            double timeBetweenPackets = 1.0 / (double)packetRate;
            
            //add packet sending and resource reserving/releasing to the actionQueue
            double currTime = work.startSec;
            int currVC = topology.getVCID();
            
            actionQueue.push(RouteAction::reserveVC(currTime, currVC, routingAlgorithm->route(work.src, work.dest))); //initially find and reserve route
            
            actionQueue.push(RouteAction::sendPacket(currTime, currVC)); //send initial packet
            
            //send through rest of packets
            for (int p = 1; p != numPackets; p++) {
                
                currTime += timeBetweenPackets;
                
                if (networkScheme == std::PACKET) {
                    //in a packet network find and reserve route for each packet seperately
                    actionQueue.push(RouteAction::releaseVC(currTime, currVC));
                    currVC = topology.getVCID(); //new VC ID
                    actionQueue.push(RouteAction::reserveVC(currTime, currVC, routingAlgorithm->route(work.src, work.dest))); //find and reserve route for this connection
                }
                
                //send thorugh packet
                actionQueue.push(RouteAction::sendPacket(currTime, currVC));
            }
            
            //release last VC resource
            actionQueue.push(RouteAction::releaseVC(work.startSec + work.timeSec, currVC));
            
            workloadPos++; //next workload
        }
        
        delete routingAlgorithm; //be free
        return stats;
    }
};



int main(int argc, const char * argv[]) {
    
    /* ~ ~ ~ 1. parse parameters ~ ~ ~ */
    
	std::string strUsage = "Usage: RoutingPerformance [NETWORK_SCHEME: CIRCUIT or PACKET] [ROUTING_SCHEME: SHP, SDP or LLP] TOPOLOGY_FILE WORKLOAD_FILE PACKET_RATE\n";
    
    if (argc != 6) {
    	std::cout << strUsage;
        return EXIT_FAILURE;
    }
    
    std::NetworkScheme network;
    std::RoutingScheme routing;
    
    if (std::string(argv[1]).compare("CIRCUIT") == 0) {
        network = std::CIRCUIT;
    } else if (std::string(argv[1]).compare("PACKET") == 0) {
        network = std::PACKET;
    } else {
        std::cout << strUsage;
        return EXIT_FAILURE;
    }
    
    if (std::string(argv[2]).compare("SHP") == 0) {
        routing = std::SHP;
    } else if (std::string(argv[2]).compare("SDP") == 0) {
        routing = std::SDP;
    } else if (std::string(argv[2]).compare("LLP") == 0) {
        routing = std::LLP;
    } else {
        std::cout << strUsage;
        return EXIT_FAILURE;
    }
    
    std::string strTopologyFile = std::string(argv[3]);
    std::string strWorkloadFile = std::string(argv[4]);
    int packetRate = std::atoi(argv[5]);
    
    if ( !(packetRate > 0) ) {
        std::cout << "Packet rate must be a positive integer.\n";
    }
    
    /* ~ ~ ~ 2. parse topology file, creating network topology ~ ~ ~ */
    
    std::NetworkTopology topology = std::NetworkTopology();
    
    std::ifstream topologyFile(strTopologyFile.c_str(), std::ifstream::in);
	
    if (!topologyFile.is_open()) {
        std::cout << "Topology file \"" + strTopologyFile + "\" could not be opened.\n";
        return EXIT_FAILURE;
    }
    
    std::string topologyLine;
    while (std::getline(topologyFile, topologyLine, '\n')) {
        //parse each line of the topology file adding each router link
        std::istringstream topologyStream(topologyLine);
        std::string router1, router2;
        int propMS, capacity;
        topologyStream >> router1 >> router2 >> propMS >> capacity;
        topology.addRouterConnection(router1, router2, propMS, capacity);
    }
    
    /* ~ ~ ~ 3. parse workload file ~ ~ ~ */
    
    std::vector<std::Work> workload = std::vector<std::Work>();
    
    std::ifstream workloadFile(strWorkloadFile.c_str(), std::ifstream::in);
	
    if (!workloadFile.is_open()) {
        std::cout << "Workload file \"" + strTopologyFile + "\" could not be opened.\n";
        return EXIT_FAILURE;
    }
    
    std::string workloadLine;
    while (std::getline(workloadFile, workloadLine, '\n')) {
        //parse each line of the workload into vector array
        std::istringstream workloadStream(workloadLine);
        std::string src, dest;
        double startSec, timeSec;
        workloadStream >> startSec >> src >> dest >> timeSec;
        std::Work w = {src, dest, startSec, timeSec};
        workload.push_back(w);
    }
    
    /* ~ ~ ~ 4. Analyse Routing Performance ~ ~ ~ */
    
    std::NetworkStats stats = std::NetworkAnalysis::analyseRoutingPerformance(topology, network, routing, workload, packetRate);
    
    /* ~ ~ ~ 5. Print Results ~ ~ ~ */

    std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
    std::cout.precision(2); //2 decimal places
    
    std::cout << "total number of virtual circuit requests: " << (stats.vcircuitRequests) << "\n";
    std::cout << "total number of packets: " << (stats.packetsBlocked + stats.packetsRouted) << "\n";
    std::cout << "number of successfully routed packets: " << (stats.packetsRouted) << "\n";
    
    if (stats.packetsBlocked + stats.packetsRouted == 0)
        std::cout << "percentage of successfully routed packets: N/A\n";
    else
        std::cout << "percentage of successfully routed packets: "
            << (((double)stats.packetsRouted) / ((double)(stats.packetsBlocked + stats.packetsRouted))) * 100.0
            << "\n";
    
    std::cout << "number of blocked packets: " << (stats.packetsBlocked) << "\n";
    
    if (stats.packetsBlocked + stats.packetsRouted == 0)
        std::cout << "percentage of blocked packets: N/A\n";
    else
        std::cout << "percentage of blocked packets: "
            << (((double)stats.packetsBlocked) / ((double)(stats.packetsBlocked + stats.packetsRouted))) * 100.0
            << "\n";
    
    if (stats.vcircuitRouted == 0) {
        std::cout << "average number of hops per circuit: 0\n";
        std::cout << "average cumulative propagation delay per circuit: 0\n";
    } else {
        std::cout << "average number of hops per circuit: "
            << (((double)stats.totalRoutedVCircuitHops) / ((double)stats.vcircuitRouted)) << "\n";
        std::cout << "average cumulative propagation delay per circuit: "
            << (((double)stats.totalRoutedVCircuitProp) / ((double)stats.vcircuitRouted)) << "\n";
    }
    
    return EXIT_SUCCESS;
}






